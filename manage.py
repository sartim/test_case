#!/usr/bin/python3

"""
This script creates sqlite databse and inserts all cav data
"""

import csv
import sqlite3
import getopt
import sys


# Create database if it does not exist
db = sqlite3.connect('data/transaction_db')
# Get a cursor object
session = db.cursor()


def main(argv):
    """
    Main function
    :param argv:
    :return:
    """
    csvfiles = ''
    n = ''

    try:
        opts, args = getopt.getopt(argv, "hi:", ["ifile="])
    except getopt.GetoptError:
        print('manage.py -i <csvfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('manage.py -i <csvfile>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            csvfiles = arg

    transaction_csv_num_sql = '''
            CREATE TABLE transaction_csvs(
                id INTEGER PRIMARY KEY AUTOINCREMENT, 
                csv_path VARCHAR UNIQUE
              )
        '''

    transactions_sql = '''
            CREATE TABLE transactions(
              id INTEGER PRIMARY KEY AUTOINCREMENT,
              account VARCHAR,
              amount INTEGER,
              transaction_date date,
              transaction_csv_id INTEGER, 
              FOREIGN KEY(transaction_csv_id) REFERENCES transaction_csvs(id)
            )
        '''

    try:
        session.execute(transaction_csv_num_sql)
        session.execute(transactions_sql)
        db.commit()
        print("Database created successfully")
    except Exception as exception:
        print(exception)

    files = csvfiles.split(",")
    # Run insert data function
    insert_data(files)


def open_csv(read_csv, file):
    """
    Open csv and insert data
    :param read_csv:
    :param file:
    :return:
    """
    # Loop through csv file
    for row in read_csv:
        print(row)
        acc_number = row[0]
        amount = row[1]
        t_date_time = row[2]
        _split = t_date_time.split("/")
        transaction_date = "{0}-{1}-{2}".format(_split[2][:4], _split[0], _split[1])

        # Get file by id
        session.execute("""
          SELECT * FROM transaction_csvs WHERE csv_path = '{0}'
        """.format(file))
        file = []
        for _csv in session:
            file.append(_csv[0])

        insert_transaction_csv_sql = '''
                                        INSERT INTO transactions(account, amount, transaction_date,
                                        transaction_csv_id)
                                        VALUES ('{0}', '{1}', '{2}', '{3}')
                                    '''.format(acc_number, amount, transaction_date, file[0])
        session.execute(insert_transaction_csv_sql)
        db.commit()

    print("Finished inserting csv data successfully for {0}".format(file))


def insert_data(files):
    """
    Insert data to sqlite db
    :param files:
    :return:
    """
    try:
        for csvfile in files:
            # Insert csv file names to db
            insert_transaction_csv_sql = """
                                            INSERT INTO transaction_csvs (csv_path)
                                            VALUES ('{csvfile}')
                                        """.format(csvfile=csvfile.strip(" "))
            session.execute(insert_transaction_csv_sql)
            db.commit()
        print("Inserted csv paths successfully")

        # Open csvfile using csv.reader
        with open("data/{0}".format(files[0].strip(" "))) as csvfile:
            read_csv = csv.reader(csvfile, delimiter=',')
            # loop thro' csv
            open_csv(read_csv, files[0].strip(" "))

        with open("data/{0}".format(files[1].strip(" "))) as csvfile:
            read_csv = csv.reader(csvfile, delimiter=',')
            # loop thro' csv
            open_csv(read_csv, files[1].strip(" "))

        with open("data/{0}".format(files[2].strip(" "))) as csvfile:
            read_csv = csv.reader(csvfile, delimiter=',')
            # loop thro' csv
            open_csv(read_csv, files[2].strip(" "))

    except Exception as exception:
        print(exception)
    finally:
        db.close()

    return ""


if __name__ == "__main__":
    main(sys.argv[1:])
