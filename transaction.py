#!/usr/bin/python3

"""
The function prints the account which has
the longest consecutive daily payments
"""

import sys
import getopt

from manage import session


def main(argv):
    """
    Main function.
    Takes two arguments parsed with flags
    :param argv:
    :return:
    """
    csvfile = ''
    n = ''

    try:
        opts, args = getopt.getopt(argv, "hi:n:", ["ifile=", "nfile="])
    except getopt.GetoptError:
        print('transaction.py -i <csvfile> -n <n>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('transaction.py -i <csvfile> -n <n>')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            csvfile = arg
        elif opt in ("-n", "--nfile"):
            n = arg

    # Get data for csv parsed
    transaction_csvs_sql = """ 
            SELECT * FROM transaction_csvs WHERE csv_path = '{0}'
           """.format(csvfile)
    session.execute(transaction_csvs_sql)
    for v in session:
        # Get count of transaction for each day without time
        daily_consecutive_count = """
            SELECT COUNT(*) AS total, account, transaction_date
            FROM transactions WHERE transaction_csv_id = {id}
            GROUP BY account, transaction_date ORDER BY account ASC
        """.format(id=v[0])
        longest_period_sql = """
            SELECT COUNT(*) as count, account FROM ({daily_consecutive_count}) GROUP BY account 
            ORDER BY count DESC LIMIT {n}
        """.format(daily_consecutive_count=daily_consecutive_count, n=int(n))
        session.execute(longest_period_sql)
        response = []
        for value in session:
            response.append(value[1].encode('ascii', 'ignore'))
        print(response)
        return response


if __name__ == "__main__":
    main(sys.argv[1:])
