Results


Input: 1
	- transactions_csv_file_path: 'transaction_data_1.csv'
	- n: 1
Expected output:
	- ['ACC2']

Input:
	- transactions_csv_file_path: 'transaction_data_2.csv'
	- n: 2
Expected output:
	- ['ACC2', 'ACC4']

Input:
	- transactions_csv_file_path: 'transaction_data_3.csv'
	- n: 3
Expected output:
	- ['ACC470', 'ACC308', 'ACC327']
