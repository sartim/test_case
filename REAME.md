**Setup**

Make sure you have Python Interpreter, preferably Python3

_Clone repo and change directory_

    $ git clone https://bitbucket.org/sartim/test_case.git
    $ cd test_case/
    
_Give permissions to setup shell script then execute_
    
    $ chmod +x setup.sh
    $ ./setup.sh
    
_Executing test cases_

    $ python transaction.py -i "transaction_data_1.csv -n 1"
    $ python transaction.py -i "transaction_data_2.csv -n 2"
    $ python transaction.py -i "transaction_data_3.csv -n 3"
    